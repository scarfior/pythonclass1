def is_even(number):
    """
    :number - integer
    :returns - True/False
    """
    if number % 2 == 0:
        return True
    else:
        return False


print is_even(2)
print is_even(3)


def is_it_zero(number):
    """
    :number - integer
    """
    if number > 0:
        print "Positive"
    elif number < 0:
        print "Negative"
    else:
        print "Zero"


is_it_zero(4)
is_it_zero(-3)
is_it_zero(0)
